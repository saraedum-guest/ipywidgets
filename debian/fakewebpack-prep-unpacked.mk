H = fakewebpack-helpers
U = fakewebpack-unpacked
B = fakewebpack

ALL = \
  $U/phosphor/lib.stamp \
  $U/phosphor/styles/base.css \
  $U/phosphor/styles/base.css?f74d \
  $U/jupyter-js-widgets/package.json \
  $U/jupyter-js-widgets/lib.stamp \
  $U/jupyter-js-widgets/css/widgets.css \
  $U/jupyter-js-widgets/css/widgets.css?7dc3 \
  $U/widgetsnbextension/webpack/bootstrap\ e65abc6196a7e23c9fcf \
  $U/widgetsnbextension/css/outputarea.css \
  $U/widgetsnbextension/css/outputarea.css?73c5 \
  $U/widgetsnbextension/package.json \
  $B/prep.stamp

all: $(ALL)

%/package.json: %/package.json.real
	printf "module.exports = $$(cat "$<");" > "$@"

# multiline Make variable can't be empty
export define NLD

d
endef
# sed script to do css @import
SED_CSS_IMPORT = -e '/@import "$(subst /,\/,$(1))"/{r $(1)'"$${NLD}"'}'

%/lib.stamp:
	cd "$(dir $@)" && tsc --project src

NODE_PATH_fakewebpack-unpacked/jupyter-js-widgets/css/widgets = ../..
NODE_PATH_fakewebpack-unpacked/widgetsnbextension/css/outputarea = ../node_modules
NODE_PATH_fakewebpack-unpacked/phosphor/styles/base = ../..

# NOTE: upstream's build uses postcss to add a bunch of crap like -ms-* and
# -webkit-* compatibility styles. We probably don't need them in Debian
# and postcss is another one of those bloated self-important web build tools.
# Ain't nobody got time to package that shit!
%.css: %.css.real
	mkdir -p "$(dir $@)" && NODE_PATH=$(NODE_PATH_$*) $H/css-loader-pack.py < "$<" > "$@"

# upstream also uses "postcss" crap here to add in backwards-compat for CSS
# variables but all browsers on Debian already support it:
# https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_variables#Browser_compatibility
$U/jupyter-js-widgets/css/widgets.css.real:
	mkdir -p "$(dir $@)"
	cd ../jupyter-js-widgets/css && cat widgets.css | \
	sed $(call SED_CSS_IMPORT,./labvariables.css) \
	    $(call SED_CSS_IMPORT,./widgets-base.css) | \
	sed $(call SED_CSS_IMPORT,./materialcolors.css) > $(CURDIR)/"$@"

%.css?73c5 %.css?f74d %.css?7dc3: %.css
	mkdir -p "$(dir $@)" && m4 -DNODE_PATH=$(NODE_PATH_$*) -DCSS_INPUT=./$(notdir $<) "$H/style-loader.js.m4" > "$@"

$U/widgetsnbextension/webpack/bootstrap%:
	mkdir -p "$(dir $@)" && m4 -DWEBPACK_PUBLIC_PATH= "$H/webpack-bootstrap.js.m4" > "$@"


$B/prep.stamp: $(STATIC_RESOURCES)
	mkdir -p $B
	touch "$@"

clean:
	rm -rf $(ALL) $U/jupyter-js-widgets/lib $U/phosphor/lib $U/jupyter-js-widgets/css/widgets.css.real

.PHONY: all clean
