#!/usr/bin/python3

import json
import os
import re
import sys

# Try to emulate what webpack+css-loader does; they treat '#xxx' parts in URLs specially
CSS_URL = re.compile(r"""url\((?:\\")?(.*?)(\??#.*?)?(?:\\")?\)""")

template = """exports = module.exports = require("%s/css-loader/lib/css-base.js")();
// imports


// module
exports.push([module.id, %%s, ""]);

// exports
""" % os.getenv("NODE_PATH")

def replace(match):
	if match.group(1).startswith("data:"):
		return match.group(0)
	path = match.group(1) if match.group(1).startswith(".") else os.path.join(".", match.group(1))
	return """url(" + require("%s") + "%s)""" % (path, match.group(2))

f = sys.stdin.read()
print(template % re.sub(CSS_URL, replace, json.dumps(f)))
