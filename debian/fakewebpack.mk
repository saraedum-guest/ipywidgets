#!/usr/bin/make -f
# Fake webpack
#
# How to use this, to update ipywidgets webpack components.
# (For a list of components, look at ALL defined below in this makefile)
#
# 1. Get the upstream build products. You can either:
#    * Build them yourself using npm(1), or
#    * Download them from PyPI or NPM or something, or
#    * `git checkout built/$version -- upstream; git reset -- upstream` if someone already did this.
# 2. Run `$0 prep` to extract their source files.
# 3. For each component, repeat the following:
#    a) `$0 FAKEWEBPACK_INTERACTIVE_RETRY=8 upstream-reproduce/%.stamp`
#    b) edit fakewebpack-meta/%.modules to respond to the error; check the old
#       version for hints on how it's formatted
#    until there are no errors. TODO: this could be automated better.
# 4. Commit your manually-edited fakewebpack-meta/%.modules into git.
# 5. For each component, repeat the following:
#    a) `$0 clean upstream-reproduce/%.diff`
#    b) edit our scripts (f-generate.py f-postprocess.js) - or perhaps
#       fakewebpack-meta/%.modules again, you might have done step (3) incorrectly -
#       to reduce any significant differences until they are reasonably minimal.
# 6. Commit the updated scripts into git.
# 7. Run `$0 clean all-diff` then check the diffs remain minimal.
# 8. Copy the tree-structure of upstream-unpacked/ into fakewebpack-unpacked/
#    except every file should be a symlink either to a Debian system file, or
#    if this unavailable then to a file in missing-sources/ that you add.
# 9. Probably, you'll also need to update fakewebpack-prep-unpacked.mk.
#    Also run `$0 check-missing-sources` to check that the bundled stuff
#    isn't out-of-date.
# A. Check fakewebpack-unpacked/ and any other updates into git.
#    You can run `$0 all-unpacked-diff` to review the differences between this
#    and upstream-unpacked/. This need not be minimal, but you should verify
#    that the general structure looks the same, especially the non-JS files.
# X. Now `$0 clean all` should build you some nice things in fakewebpack/*, and
#    you can call this from debian/rules.
#
# NOTES:
#
# - The "Bottom edge touching"/"Right edge touching" extra diff is this bug:
#   https://github.com/benjamn/recast/issues/342
#   For us, it's not worth worrying about.
# - widgetsnbextension embeds two different copies of Backbone. We deduplicate
#   this and so there are some extra diffs about "var Backbone = __webpack_require__(3)"
#   instead of "...(24)"; the numbers may be a bit different after 5.2.2.

# FIXME: build jupyter-js-widgets too, it has extra dependencies
#ALL = jupyter-js-widgets widgetsnbextension
ALL = widgetsnbextension
TARGET_jupyter-js-widgets = embed.js
TARGET_widgetsnbextension = extension.js
ISAMD_jupyter-js-widgets = False
ISAMD_widgetsnbextension = True

DIFF = diff -wB -U5

FAKEWEBPACK = \
	rm -rf "$(1)/$*" && mkdir -p "$(1)/$*" && \
	./fakewebpack-generate.py fakewebpack-meta/$*.files fakewebpack-meta/$*.modules $(2)/$*/ $(ISAMD_$*) > "$(1)/$*/$(TARGET_$*)" && \
	touch "$@"

all: $(ALL:%=fakewebpack/%.stamp)

all-diff: $(ALL:%=upstream-reproduce/%.diff)

all-unpacked-diff: $(ALL:%=fakewebpack/%-unpacked.diff)

$(ALL:%=fakewebpack/%-unpacked.stamp): \
  fakewebpack-prep-unpacked.mk \
  $(ALL:%=fakewebpack-meta/%.files) \
  $(ALL:%=fakewebpack-meta/%.modules) \
  $(ALL:%=fakewebpack-unpacked/%/)
	$(MAKE) -f "$<" all
	touch $(ALL:%=fakewebpack/%-unpacked.stamp)

fakewebpack/%.stamp: fakewebpack/%-unpacked.stamp
	$(call FAKEWEBPACK,fakewebpack,fakewebpack-unpacked)
	mkdir -p fakewebpack && touch "$@"

fakewebpack/%.packages: fakewebpack-meta/%.files
	mkdir -p fakewebpack
	cat fakewebpack-meta/$*.files | \
	  while read x; do \
	    y="$$(readlink -f "fakewebpack-unpacked/$*/$$x")"; \
	    case "$$y" in \
	    /usr/*) dpkg -S "$$y";; \
	    esac; \
	  done | \
	  cut -d: -f1 | \
	  sort -u | \
	  xargs -rn1 dpkg-query -f '$${source:Package} (= $${source:Version}), ' -W > "$@"

fakewebpack/%-unpacked.diff: fakewebpack/%-unpacked.stamp
	mkdir -p fakewebpack
	{ f="fakewebpack-meta/$*.files"; grep -v '\.js$$' "$$f"; grep '\.js$$' "$$f"; } | while read x; do \
		$(DIFF) fakewebpack-unpacked/$*/"$$x" upstream-unpacked/$*/"$$x"; \
	done > "$@" || true
	@echo "If diff gave any stderr output like 'No such file or directory' then you need to fix"
	@echo "either the tree structure in fakewebpack-unpacked/* or fakewebpack-prep-unpacked.mk"

upstream-reproduce/%.stamp: fakewebpack-meta/%.files fakewebpack-meta/%.modules upstream-unpacked/%/
	$(call FAKEWEBPACK,upstream-reproduce,upstream-unpacked)

upstream-reproduce/%.diff: upstream-reproduce/%.stamp
	$(DIFF) -r upstream/$*/$(TARGET_$*) upstream-reproduce/$*/$(TARGET_$*) > "$@" || true

# jupyter-js-widgets still uses an old script.js, we have the bugfix:
# https://github.com/ded/script.js/commit/ac14b4fc0f0db21620f7b7b99e8a5e2967e8ccdf
check-missing-sources:
	cd missing-sources && find . -type f -exec \
	  $(DIFF) -r '{}' '../upstream-unpacked/jupyter-js-widgets/node_modules/{}' \;

clean:
	$(MAKE) -f fakewebpack-prep-unpacked.mk clean
	rm -rf upstream-reproduce fakewebpack

prep: $(ALL:%=fakewebpack-meta/%.files) $(ALL:%=fakewebpack-meta/%.modules)

fakewebpack-meta/%.modules: fakewebpack-meta/%.files
	tail -n+2 "$<" > "$@"

fakewebpack-meta/%.files: upstream/%
	./fakewebpack-extract-source-map.py upstream/$*/$(TARGET_$*).map \
	  upstream-unpacked/$*/ > fakewebpack-meta/$*.files

.PHONY: all all-diff clean prep
